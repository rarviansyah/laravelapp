<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\CartController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/web', function(){
    return view('index');
});

Route::group(['middleware' => 'guest'], function() {
    Route::get('/register', [AuthController::class, 'register'])->name('register');
    Route::post('/register', [AuthController::class, 'registerPost'])->name('register');
    Route::get('/login', [AuthController::class, 'login'])->name('login');
    Route::post('/login', [AuthController::class, 'loginPost'])->name('login');
});

Route::group(['middleware' => 'auth'], function() {
    Route::get('/home', [HomeController::class, 'index']);
    Route::post('logout', [AuthController::class, 'logout'])->name('logout');
    
    Route::group(['prefix' => 'product', 'as' => 'product'], function() {
            Route::get('/', [ProductController::class, 'index'])->name('index');
            
        });

    Route::get('/create', [ProductController::class, 'create'])->name('create');
    Route::post('product/create', [ProductController::class, 'store'])->name('product.store');
    Route::get('myproduct', [ProductController::class, 'myproduct'])->name('product.myproduct');
    
    Route::get('/home', [CartController::class, 'index']);
    Route::get('/cart', [CartController::class, 'cart'])->name('cart');
    Route::get('/cart/{id}', [CartController::class, 'addtocart'])->name('add.to.cart');
    // Route::post('/cart/remove', [CartController::class, 'removeFromCart'])->name('remove');
    Route::get('/buy/{id}', [CartController::class, 'buy'])->name('buy');
    Route::get('/purchase', [CartController::class, 'purchase'])->name('purchase');
    

});
Route::group(['prefix' => 'profile', 'as' => 'profile'], function(){
    Route::get('/', [ProfileController::class, 'index'])->name('index');    
});
Route::get('/profile', [AuthController::class, 'profile'])->name('profile');


// Auth::routes();  

Route::middleware(['auth', 'user-access:user'])->group(function() {
    Route::get('/home', [HomeController::class, 'index'])->name('home');
});

Route::middleware(['auth', 'user-access:admin'])->group(function() {
    Route::get('/admin/home', [HomeController::class, 'adminHome'])->name('admin.home');
});




