<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class cart extends Model
{
    use HasFactory;

    protected $dates = ['created_at'];

    protected $fillable = ['product_id', 'name', 'price', 'image'];

    public function detail(){
        return $this->belongsTo(Product::class, 'product_id');
    }
}
