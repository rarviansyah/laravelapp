<?php

namespace App\Http\Controllers;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\ProductSold;

class ProductController extends Controller
{
    public function index(Request $request){
        $products = Product::orderBy('sold', 'ASC');
        if($request->sort == 'asc'){
            $products =  $products->orderBy('name', 'ASC')->get();
        }else if($request->sort == 'desc'){
            $products =  $products->orderBy('name', 'DESC')->get();
        }else{
            $products =  $products->orderBy('created_at', 'DESC')->orderBy('updated_at', 'DESC')->get();
        }

        return view('pages.products', compact('products'));
    }

    public function create(){
        return view('pages.create');
    }

    public function store(Request $request){
        if($request->price < 1){
            return back()->with('error', 'Minimum price is $. 1');
        }

        $file = $request->file('image');
        $fileName = $file->getClientOriginalName(); 
        $file->move(public_path('images'), $fileName); 
 
        Product::create([
            'name' => $request->name,
            'image' => $fileName,
            'description' => $request->description,
            'price' => $request->price,
            'sold' => "0",
            'user_id' => Auth::user()->id,
        ]);

        return back()->with('success', 'Product has been successfully created.');
    }

    // public function buy($id){
    //     $product = Product::findOrFail($id);
 
    //     if (!$product) {
    //         return back()->with('error', 'Product not found.');
    //     }

    //     ProductSold::create([
    //         'product_id' => $product->id,
    //         'buyer_id' => Auth::user()->id,
    //     ]);
        
    //     $product->update([
    //         'sold' => true,
    //     ]);
        
    //     return back()->with('success', 'Product has been purchased successfully.');
    // }

    // public function myproduct(){
    //     $products = Product::where('user_id', Auth::user()->id)->orderBy('sold', 'asc')->get();
    //     return view('pages.myproduct', compact('products'));
    // }    
}
 