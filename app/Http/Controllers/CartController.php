<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductSold;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function index(Request $request)
    {
        $products = Product::all();
        return view('cart', compact('product'));
    }

    public function cart()
    {
        return view('cart');
    }

    public function addtocart($id)
    {
        $product = Product::findOrFail($id);
        $cart = session()->get('cart', []); 
        
        if(isset($cart[$id])) {
            $cart[$id];
        } else {
            $cart[$id] = [
                'product_id' => $product->id,
                'name' => $product->name,
                'price' => $product->price,
                'created_at' => $product->created_at,
                'image' => $product->image
            ];
        }
        
        session()->put('cart', $cart);
        return redirect()->back()->with('success', 'Product has been add to cart');
    }
  
    public function buy($id){
        $product = Product::findOrFail($id);
        
        if (!$product) {
            return back()->with('error', 'Product not found.');
        }
        // session()->forget('cart.'.$product['product_id']);
    
        // return back()->with('success', 'Product has been purchased successfully.');
        return redirect()->route('purchase')->with('success', 'Product has been purchased successfully.');
    }

    public function purchase(){
        // return view('pages.purchase');
        $cart = session('cart');
        return view('pages.purchase', compact('cart'));
    }
    
}
