<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }

    public function registerPost(Request $request){ 
        $request->validate([
            'name' => ['required', 'string', 'min:5'],
            'email' => ['required', 'email'],
            'password' => ['required', 'min:6'],
            'phone_number' => ['required', 'numeric'],
            'gender' => ['required', 'in:male,female'],
        ]);
        
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);        
        $user->phone_number = $request->phone_number;        
        $user->gender = $request->gender;        
        
        $user->save();

        return back()->with('success', 'Register Successfully');
    }

    public function login(){ 
        return view('login');
    }

    public function loginPost(Request $request){
        $credetials = [
            'email' => $request->email,
            'password' => $request->password,
        ];

        if (Auth::attempt($credetials)) {
            $user = Auth::user();
            if ($user->type == 'admin') {
                return redirect()->route('admin.home');
            } else {
                return redirect()->route('home');
            }
        } else {
            return back()->with('error', 'Email or Password Error');
        }
    }

    public function logout(){
        Auth::logout();

        return redirect()->route('login');
    }

    public function profile(){
        return view('pages.profile');
    }
}
