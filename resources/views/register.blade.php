<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Register</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
</head>
<body>
  <div class="row justify-content-center mt-5">
    <div class="col-lg-4">
      <div class="card">
        <div class="card-header">
          <h1 class="card-title text-center" >Register</h1>
        </div>
        <div class="card-body">
          @if (Session::has('success'))
              <div class="alert alert-success" role="alert">
                {{Session::get('success')}}
              </div>
              <p class="text-center"> <a href="{{'login'}}">Login</a></p>

          @endif
          <form action="{{ route('register') }}" method="POST">
            @csrf
            <div class="mb-3">
                <label for="name" class="form-label">Name</label>
                <input type="text" name="name" id="name" class="form-label" autofocus required>
            @error('name')
                <div class="text-danger mt-2">
                    {{ $message}}
                </div>
            @enderror
            </div>
            <div class="mb-3">
                <label for="email" class="form-label">Email</label>
                <input type="text" name="email" id="email" class="form-label" autofocus required >
                @error('email')
                <div class="text-danger mt-2">
                    {{ $message}}
                </div>
            @enderror
            </div>
            <div class="mb-3">
                <label for="password" class="form-label">Password</label>
                <input type="password" name="password" id="password" class="form-label" autofocus required>
                @error('password')
                <div class="text-danger mt-2">
                    {{ $message}}
                </div>
            @enderror
            </div>
            <div class="mb-3">
                <label for="phone_number" class="form-label">No Phone</label>
                <input type="number" name="phone_number" id="phone_number" class="form-label" autofocus required>
                @error('phone_number')
                <div class="text-danger mt-2">
                    {{ $message}}
                </div>
            @enderror
            </div>
            <div class="mb-3">
                <label for="gender" class="form-label">Gender</label>
                <select class="form-select" name="gender" id="gender" aria-label="Default select example" autofocus required>
                  <option selected>Select Gender</option>
                  <option value="female">Female</option>
                  <option value="male">Male</option>
                </select>
                @error('gender')
                <div class="text-danger mt-2">
                    {{ $message}}
                </div>
            @enderror            
              </div>
            <div class="mb-3">
              <div class="d-grid">
                <button class="btn btn-info" type="submit">Register</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  
</body>
</html>