@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body p-3">
                    <h5>Cart</h5>
                    <table class="table align-items-center mb-0">
                        <thead>
                            <tr>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Product</th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Price</th>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (session('cart'))
                            @foreach(session('cart') as $id => $product)
                                <tr>
                                    <td>
                                        <div class="d-flex align-items-center">
                                            @if ($product)
                                            <input type="checkbox" class="product-checkbox" data-price="{{ $product['price'] }}">
                                            @if (isset($product['image']))
                                            <img src="{{ asset('images/' . $product['image']) }}" style="height: 50px; width: 50px; object-fit: contain"/>
                                            @else
                                            <p>Image Not Found</p>
                                            @endif
                                            <div class="ms-2">
                                                <h6 class="mb-0 text-sm">{{ $product['name'] }}</h6>
                                            </div>
                                            @else
                                            <p>Data Product Not Available</p>
                                            @endif
                                        </div>
                                    </td>
                                    <td>
                                        <div class="d-flex">
                                            <div class="d-flex flex-column justify-content-center">
                                                <h6 class="mb-0 text-sm text-primary">Rp {{ number_format($product['price'], 0, '.', '.') }}</h6>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="d-flex flex-column justify-content-center">
                                            @if (isset($product['created_at']))
                                            <h6 class="mb-0 text-sm text-primary">{{ $product['created_at'] }}</h6>
                                            @else
                                            <p>Created date not available</p>
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-3 mt-3">
            <div class="card">
                <div class="card-body p-3 text-center">
                    <h5>Checkout</h5>
                    <table class="table align-items-center mb-0">
                        <thead>
                            <tr>
                                <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2 total-purchases">Total Purchases</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    @if (count(session('cart')) > 0)
                                    <a href="{{ route('buy', $product['product_id']) }}" class="btn bg-gradient-primary">Buy</a>
                                    @else
                                    <p>Cart Is Empty</p>
                                    @endif
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

{{-- @section('script')
<script>
    // Ketika halaman dimuat, sembunyikan semua elemen total pembelian
    document.querySelector('.total-purchases').style.display = 'none';

    // Ambil semua checkbox
    const checkboxes = document.querySelectorAll('.product-checkbox');

    // Tambahkan event listener untuk menghitung total pembelian saat checkbox diberi tanda centang
    checkboxes.forEach((checkbox) => {
        checkbox.addEventListener('change', () => {
            let totalPurchases = 0;

            checkboxes.forEach((cb) => {
                if (cb.checked) {
                    const price = parseFloat(cb.getAttribute('data-price'));
                    totalPurchases += price;
                }
            });

            // Tampilkan total pembelian di elemen dengan class "total-purchases"
            const totalPurchasesElement = document.querySelector('.total-purchases');
            totalPurchasesElement.textContent = 'Total Purchases: Rp ' + totalPurchases.toFixed(2);
            totalPurchasesElement.style.display = 'block';
        });
    });
</script>
@endsection --}}
@section('script')
<script>
    // Ketika halaman dimuat, sembunyikan semua elemen total pembelian
    document.querySelector('.total-purchases').style.display = 'none';

    // Ambil semua checkbox
    const checkboxes = document.querySelectorAll('.product-checkbox');

    // Tambahkan event listener untuk menghitung total pembelian saat checkbox diberi tanda centang
    checkboxes.forEach((checkbox) => {
        checkbox.addEventListener('change', () => {
            let totalPurchases = 0;
            let isChecked = false;

            checkboxes.forEach((cb) => {
                if (cb.checked) {
                    const price = parseFloat(cb.getAttribute('data-price'));
                    totalPurchases += price;
                    isChecked = true;
                }
            });

            // Tampilkan total pembelian atau pesan kesalahan di elemen dengan class "total-purchases"
            const totalPurchasesElement = document.querySelector('.total-purchases');
            
            if (isChecked) {
                totalPurchasesElement.textContent = 'Total Purchases: Rp ' + totalPurchases.toFixed(2);
                totalPurchasesElement.style.display = 'block';
            } else {
                totalPurchasesElement.textContent = 'Please select at least one product.';
                totalPurchasesElement.style.display = 'block';
            }
        });
    });
</script>
@endsection
