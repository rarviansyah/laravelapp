@extends('layouts.app')

@section('content')
<div class="container-fluid">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h2>Product</h2>
    </div>
  </div>
</div>

<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-2">
        <div class="d-flex align-items-center justify-content-center">
          <p class="mb-0">Sort By</p>
          {{-- <select class="form-control w-40" id="sortBy text-end">
            <option value=""  {{ request()->sort == '' ? 'selected' : '' }}>Latest</option>
            <option value="asc" {{ request()->sort == 'asc' ? 'selected' : '' }}>Ascending</option>
            <option value="desc" {{ request()->sort == 'desc' ? 'selected' : '' }}>Descending</option>
        </select> --}}
        </div>
      </div>
    </div>
    <div class="row mt-4">
      @if ($products->count() > 0)
        @foreach($products as $product)
        <div class="col-xl-4 col-sm- mb-6">
            <div class="card ">
            <div class="card-header text-center">
                <img src="{{asset('images/' . $product->image) }}" style="height: 150px; width: 100%; object-fit: contain;">
            </div>
            <div class="card-body p-3">
              <div class="row">
              <div class="col-8">
                  <div class="numbers">
                  {{-- <small class="d-flex align-items-center text-capitalize">
                      <i class="ri-store-2-fill me-1"></i> 
                      <span>{{ $product->seller->name }}</span>
                      @if ($product->seller)
                        <span>{{ $product->seller->name }}</span>
                      @else
                        <span>Seller Not Available</span>
                      @endif
                  </small> --}} 
                  <p class="mb-0 text-capitalize font-weight-bold ">{{ $product->name }}</p>
                  <h5 class="font-weight-bolder mb-0">
                    {{ 'Rp ' . number_format($product->price, 0, ',', '.') }}
                  </h5>
                  <small>{{ $product->description }}</small>
                  </div>
              </div>
              <div class="col-4 text-end">
              {{-- @if(!$product->sold) --}}
              <a href="{{route('add.to.cart', $product->id)}}" class="btn bg-gradient-primary">Add To Cart</a>
                  {{-- @else
                  <span class="btn bg-gradient-danger">Sold</span>
                  @endif --}}
              </div>
              </div>
          </div>
            </div>
        </div>
        @endforeach
      @else
          <div class="col-12">
            <div class="card">
              <div class="card-body text-center p-3">
                <h4>Product Not Available</h4>
              </div>
            </div>
          </div>
      @endif
    </div>
  </div>
</section>
@endsection
