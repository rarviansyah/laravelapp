@extends('layouts.app')

@section('content')
    <div class="container-fluid py-2">
      <div class="row">
        <div class="col-12 col-lg-6">
          <div class="card">
            <div class="card-body p-3">
              <h5>Add Product</h5>
              <form action="{{ route('product.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="name" class="form-control-label">Name</label>
                    <input type="text" name="name" class="form-control" id="name" required>
                </div>
                <div class="form-group">
                    <label for="image" class="form-control-label">Image</label>
                    <input type="file" name="image" class="form-control form-control-file" id="image" accept=".jpg, .png, .svg" required>
                </div>
                <div class="form-group">
                    <label for="description" class="form-control-label">Description</label>
                    <textarea rows="6" name="description" class="form-control" id="description" required></textarea>
                </div>
                <div class="form-group">
                  <label for="price" class="form-control-label">Price</label>
                  <div class="input-group">
                      <span class="input-group-text">Rp</span>
                      <input type="number" name="price" class="form-control" id="price" required min="1">
                  </div>
                  <small>* Minimal Rp1</small>
              </div>              
                <div class="form-group">
                    <button class="btn btn-primary">Add</button>
                </div>
            </form>
            <style>
              /* Tambahkan garis pada label */
              .form-group label::after {
                  content: "";
                  display: block;
                  width: 100%;
                  height: 2px;
                  background-color: #2d2b2b; /* Warna garis sesuaikan dengan preferensi Anda */
                  margin-top: 5px; /* Jarak antara label dan garis */
              }
          </style>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection