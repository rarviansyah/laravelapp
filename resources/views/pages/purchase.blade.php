@extends('layouts.app')

@section('content')
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body p-3">
              <h5>Purchase History</h5>
              <hr>
              @if (count($cart) > 0)
              <table class="table align-items-center mb-0">
                <thead>
                  <tr>
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Product</th>                    
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Price</th>                    
                    <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Date</th>
                  </tr>
                </thead>
                <tbody>                  
                  @foreach ($cart as $product)
                    <tr>
                      <td>
                        <div class="d-flex align-items-center">
                          @if ($product)
                            @if (isset($product['image']))
                                <img src="{{ asset('images/' . $product['image']) }}" style="height: 50px; width: 50px; object-fit: contain"/>
                            @else
                                <p>Image Not Found</p>
                            @endif
                            <div class="ms-2">
                              <h6 class="mb-0 text-sm">{{ $product['name'] }}</h6>
                            </div>
                          @else
                            <p>Data Product Not Available</p>
                          @endif
                        </div>
                      </td>
                      <td>
                        <div class="d-flex">
                          <div class="d-flex flex-column justify-content-center">
                            <h6 class="mb-0 text-sm text-primary">Rp {{ number_format($product['price'], 0, '.', '.') }}</h6>
                          </div>
                        </div>
                      </td>
                      <td>
                        <div class="d-flex flex-column justify-content-center">
                          @if (isset($product['created_at']))
                          <h6 class="mb-0 text-sm text-primary">{{ $product['created_at'] }}</h6>
                        @else
                          <p>Created date not available</p>
                        @endif
                        </div>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
              @else
                <p>Your cart is empty</p>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection
